module.exports = {
  lintOnSave: process.env.NODE_ENV !== "production",
  pwa: {
    name: "Ayelen Guerra"
  }
};
